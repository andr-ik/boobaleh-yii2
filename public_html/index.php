<?php

$servers = [
    'admin.boobaleh.dev' => 'backend',
    'admin.boobaleh.ru' => 'backend',
    'boobaleh.dev' => 'frontend',
    'boobaleh.ru' => 'frontend',
];

$app = 'frontend';
$server = $_SERVER['SERVER_NAME'];
if(isset($servers[$server])) {
    $app = $servers[$server];
}

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../common/config/bootstrap.php');
require(__DIR__ . '/../'.$app.'/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../common/config/main.php'),
    require(__DIR__ . '/../common/config/local/'.YII_ENV.'-main-local.php'),
    require(__DIR__ . '/../'.$app.'/config/main.php'),
    require(__DIR__ . '/../'.$app.'/config/local/'.YII_ENV.'-main-local.php')
);

$application = new yii\web\Application($config);
$application->run();
