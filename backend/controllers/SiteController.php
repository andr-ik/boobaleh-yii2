<?php
namespace backend\controllers;

use Yii;
use backend\controllers\base\BackendController;

class SiteController extends BackendController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index.twig');
    }
}
