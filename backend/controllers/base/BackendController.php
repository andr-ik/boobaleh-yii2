<?php
namespace backend\controllers\base;

use Yii;
use yii\web\Controller;

class BackendController extends Controller
{
    public $layout = 'main.twig';
}
