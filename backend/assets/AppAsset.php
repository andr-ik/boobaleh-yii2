<?php
namespace backend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/web';

    public $depends = [
        'yii\web\YiiAsset',
        'common\assets\BrowserSyncAsset'
    ];

    public $css = [
        'css/main.css',
    ];

    public $js = [
        'js/main.js'
    ];
}
