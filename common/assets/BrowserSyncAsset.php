<?php
namespace common\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

class BrowserSyncAsset extends AssetBundle
{
    public function init()
    {
        if (YII_DEBUG){
            Yii::$app->view->registerJsFile("http://localhost:3002/browser-sync/browser-sync-client.js", [
                'position' => View::POS_END,
                'async' => true
            ]);
        }
        parent::init();
    }
}
