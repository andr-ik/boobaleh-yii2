<?php

namespace common\generators\model;

use yii;

class Generator extends \yii\gii\generators\model\Generator
{
    public function formView()
    {
        return Yii::getAlias('@vendor/yiisoft/yii2-gii/generators/model/form.php');
    }

    public function convertToConst($field){
        return 'FIELD_'.mb_strtoupper($field);
    }

    public function generateRules($table){
        $rules = parent::generateRules($table);
        $columns = $this->getColumns($table);
        $return = [];
        foreach($rules as $rule){
            $matches = [];
            preg_match_all('/\[(\'.*?\')\]/i', $rule, $matches);
            $from = $matches[0];
            $to = [];
            foreach(explode(',', $matches[1][0]) as $col) {
                $col = trim(trim($col),'\'');
                if(in_array($col,$columns)){
                    $to[] = '   self::'.$this->convertToConst($col);
                }
            }
            $separator = PHP_EOL.'            ';
            $to = '['.$separator.implode(','.$separator, $to).$separator.']';
            $rule = str_replace($from, $to, $rule);
            $return[] = $rule;
        }
        return $return;
    }

    public function generateRelations(){
        $relations = parent::generateRelations();
        foreach($relations as &$relation){
            foreach($relation as $table => &$rel){
                $matches = [];
                preg_match_all('/\[(\'.*?\')\s*=>\s*(\'.*?\')\]/i', $rel[0], $matches);
                $from = $matches[0];
                $to  = $rel[1].'::'.$this->convertToConst(trim($matches[1][0],'\''));
                $to .= " => ";
                $to .= 'self::'.$this->convertToConst(trim($matches[2][0],'\''));
                $rel[0] = str_replace($from, '['.$to.']', $rel[0]);
            }
        }
        return $relations;
    }

    private function getColumns($table){
        return array_map(function($column){
            return $column->name;
        }, $table->columns);
    }
}