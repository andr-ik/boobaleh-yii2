<?php

require(__DIR__.'/di.php');

$config = [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'assetManager' => [
            'linkAssets' => true
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => false, //'@runtime/Twig/cache',
                    'options' => [
                        'auto_reload' => true,
                        'debug' => YII_DEBUG ? true : false
                    ],
                    'globals' => [
                        'html' => '\yii\helpers\Html'
                    ],
                    'uses' => [
                        'yii\bootstrap'
                    ],
                    'filters' => [
                        'dump' => 'var_dump'
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'suffix' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
];

if (YII_ENV === 'dev') {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'model' => [
                'class' => 'common\generators\model\Generator',
                'tableName' => '*',
                'ns' => 'common\models\records',
                'useTablePrefix' => true,
                'enableI18N' => true,
                'messageCategory' => 'model'
            ],
        ]
    ];
}

return $config;
