<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\BrowserSyncAsset'
    ];

    public $css = [
        'css/main.css',
    ];

    public $js = [
        'js/main.js'
    ];
}
