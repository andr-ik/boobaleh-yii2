<?php
namespace frontend\controllers;

use Yii;
use frontend\controllers\base\FrontendController;

class SiteController extends FrontendController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index.twig', ['username' => 'Andrey']);
    }
}
