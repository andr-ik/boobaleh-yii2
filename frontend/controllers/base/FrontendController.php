<?php
namespace frontend\controllers\base;

use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{
    public $layout = 'main.twig';
}
