'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();

var plumber = require('gulp-plumber');
var notify = require("gulp-notify");

var concat = require('gulp-concat');
var rename = require("gulp-rename");
var del = require('del');

var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

var uglify = require('gulp-uglify');

gulp.Gulp.prototype.__runTask = gulp.Gulp.prototype._runTask;
gulp.Gulp.prototype._runTask = function(task) {
    this.currentTask = task;
    this.__runTask(task);
};

var config = {
    host: 'localhost',
    port: 3002,
    logo: './public_html/images/logo.png',
    projects: [
        {
            name: 'frontend',
            scss: {
                src:  './frontend/web/source/scss/**/*.scss',
                dist: './frontend/web/css',
                main: './frontend/web/css/main.css'
            },
            js: {
                src:  './frontend/web/source/js/**/*.js',
                dist: './frontend/web/js',
                main: './frontend/web/js/main.js'
            }
        },
        {
            name: 'backend',
            scss: {
                src:  './backend/web/source/scss/**/*.scss',
                dist: './backend/web/css',
                main: './backend/web/css/main.css'
            },
            js: {
                src:  './backend/web/source/js/**/*.js',
                dist: './backend/web/js',
                main: './backend/web/js/main.js'
            }
        }
    ]
};

var errorHandler = {
    errorHandler: notify.onError({
        icon: config.logo,
        title: 'Ошибка в плагине <%= error.plugin %>',
        message: "Ошибка: <%= error.message %>"
    })
};

var getCurrentConfig = function(name){
    name = name.split(':')[0];
    return config.projects.filter(function(project){
        return project.name === name;
    })[0];
};

var getCurrentTask = function(name){
    return name.substring((name.indexOf(':') + 1));
};

var tasks = {
    'css': {
        deps: [],
        func: function(current){
            return gulp.src(current.scss.src)
                .pipe(plumber(errorHandler))
                .pipe(sourcemaps.init())
                .pipe(sass())
                .pipe(autoprefixer())
                .pipe(sourcemaps.write('./'))
                .pipe(gulp.dest(current.scss.dist))
        }
    },
    'css:min': {
        deps: ['css'],
        func: function(current){
            return gulp.src([
                    current.scss.main
                ])
                .pipe(plumber(errorHandler))
                .pipe(cleanCSS({compatibility: 'ie8'}))
                .pipe(rename({
                    suffix: '.min'
                }))
                .pipe(gulp.dest(current.scss.dist))
        }
    },
    'js': {
        deps: [],
        func: function(current){
            var mainFile = current.js.main.substring((current.js.main.lastIndexOf('/') + 1));
            return gulp.src(current.js.src)
                .pipe(plumber(errorHandler))
                .pipe(sourcemaps.init())
                .pipe(concat(mainFile))
                .pipe(sourcemaps.write('./'))
                .pipe(gulp.dest(current.js.dist));
        }
    },
    'js:min': {
        deps: ['js'],
        func: function(current){
            return gulp.src([
                    current.js.main
                ])
                .pipe(plumber(errorHandler))
                .pipe(uglify())
                .pipe(rename({
                    suffix: '.min'
                }))
                .pipe(gulp.dest(current.js.dist));
        }
    },
    'clear': {
        deps: [],
        func: function(current){
            var dists = [];
            dists.push(current.scss.dist + '/**/*');
            dists.push(current.scss.dist + '/!.gitignore');
            dists.push(current.js.dist + '/**/*');
            dists.push(current.js.dist + '/!.gitignore');
            del(dists);
        }
    },
    'compiler': {
        deps: ['css:min', 'js:min']
    },
    'watch': {
        deps: ['compiler','default:createServer'],
        func: function(current){
            gulp.watch(current.scss.src, [current.name+':css:min']);
            gulp.watch(current.js.src,   [current.name+':js:min']);
        }
    }
};

for(var task in tasks){
    if(tasks.hasOwnProperty(task)){
        var deps = [];
        for(var project in config.projects){
            if(config.projects.hasOwnProperty(project)){
                var projectName = config.projects[project].name;
                deps.push(projectName  + ':' + task);
                gulp.task(projectName  + ':' + task, tasks[task].deps.map(function(dep){
                    return dep.indexOf('default') === 0 ? dep : projectName + ':' + dep;
                }), function(){
                    var current = getCurrentConfig(this.currentTask.name);
                    if(!current){
                        return;
                    }
                    if(tasks[getCurrentTask(this.currentTask.name)].func){
                        return tasks[getCurrentTask(this.currentTask.name)].func(current);
                    }
                });
            }
        }
        gulp.task('all:'+task, deps);
    }
}

var createServer = function(files){
    browserSync.init({
        host: config.host,
        port: config.port,
        online: false,
        scriptPath: function (path, port, options) {
            return options.getIn(['urls', 'local']) + "/browser-sync/browser-sync-client.js";
        },
        files: files
    });
};

gulp.task('default:createServer', function(){
    var files = [];
    for(var project in config.projects){
        if(config.projects.hasOwnProperty(project)){
            files.push(config.projects[project].scss.main);
            files.push(config.projects[project].js.main);
        }
    }
    createServer(files);
});

gulp.task('default', ['all:watch']);