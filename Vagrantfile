require 'yaml'
require 'fileutils'

domains = {
  frontend: 'boobaleh.dev',
  backend:  'admin.boobaleh.dev'
}

config = {
  local: './vagrant/config/vagrant-local.yml',
  example: './vagrant/config/vagrant-local.example.yml'
}

# copy config from example if local config not exists
FileUtils.cp config[:example], config[:local] unless File.exist?(config[:local])
# read config
options = YAML.load_file config[:local]


# check github token
if options['github_token'].nil? || options['github_token'].to_s.length != 40
  puts "You must place REAL GitHub token into configuration:"
  puts "./vagrant/config/vagrant-local.yml"
  exit
end

# vagrant configurate
Vagrant.configure("2") do |config|

  config.vm.define options['machine_name']

  config.vm.box = 'ubuntu/trusty64'
  config.vm.box_check_update = options['box_check_update']

  config.vm.provider 'virtualbox' do |vb|
    vb.cpus   = options['cpus']
    vb.memory = options['memory']
    vb.name   = options['machine_name']
  end

  config.vm.network 'private_network', ip: options['ip']

  # sync: folder './' (host machine) -> folder '/var/www/app' (guest machine)
  config.vm.synced_folder './', '/var/www/app', owner: 'vagrant', group: 'vagrant'
  # disable folder '/vagrant' (guest machine)
  config.vm.synced_folder '.', '/vagrant', disabled: true

  # provisioners
  config.vm.provision 'shell', path: './vagrant/provision/once-as-root.sh',    args: [options['timezone']]
  config.vm.provision 'shell', path: './vagrant/provision/once-as-vagrant.sh', args: [options['github_token']], privileged: false
  config.vm.provision 'shell', path: './vagrant/provision/always-as-root.sh',  run: 'always'

  # post-install message (vagrant console)
  config.vm.post_up_message = "Frontend URL: http://#{domains[:frontend]}\nBackend URL: http://#{domains[:backend]}"
end
